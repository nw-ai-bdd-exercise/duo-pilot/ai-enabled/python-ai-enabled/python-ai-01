"""api_client unit tests."""
import sys
import unittest

sys.path.append("../")

from api_client import (
    authorize_consent,
    bill_pay,
    exchange_authorization_code_for_access_token,
    get_access_token,
    get_accounts,
    get_payees,
    grant_consent,
)


class TestApiClient(unittest.TestCase):
    """api_client test case."""

    def setUp(self):
        self.access_token = get_access_token()
        self.consent_id = grant_consent(self.access_token)
        self.authorization_code = authorize_consent(self.consent_id)
        self.authorized_access_token = exchange_authorization_code_for_access_token(
            self.authorization_code
        )

    def test_get_access_token(self):
        """Test get_access_token."""
        self.assertIsNotNone(self.access_token)

    def test_grant_consent(self):
        """Test grant_consent."""
        self.assertIsNotNone(self.consent_id)

    def test_authorize_consent(self):
        """Test authorize_consent."""
        self.assertIsNotNone(authorize_consent(self.authorization_code))

    def test_exchange_authorization_code_for_access_token(self):
        """Test exchange_authorization_code_for_access_token."""
        self.assertIsNotNone(self.authorized_access_token)

    def test_get_accounts(self):
        """Test get_accounts."""
        self.assertIsNotNone(get_accounts(self.authorized_access_token))

    def test_get_payees(self):
        """Test get_payees."""
        self.assertIsNotNone(get_payees(self.authorized_access_token))

    def test_bill_pay(self):
        """Test bill_pay."""
        self.assertIsNotNone(bill_pay(self.authorized_access_token))
