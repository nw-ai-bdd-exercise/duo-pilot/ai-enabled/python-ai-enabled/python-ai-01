from api_client import (
    authorize_consent,
    bill_pay,
    exchange_authorization_code_for_access_token,
    get_access_token,
    get_accounts,
    get_payees,
    grant_consent,
)

print("Start")

print("Get access token")

access_token = get_access_token()

print("access_token: ", access_token)

print("Grant consent")

consent_id = grant_consent(access_token)

print("consent_id: ", consent_id)

print("Authorize consent")

authorization_code = authorize_consent(consent_id)

print("authorization_code: ", authorization_code)

print("Exchange authorization code for access token")

authorized_access_token = exchange_authorization_code_for_access_token(authorization_code)

print("authorized_access_token: ", authorized_access_token)

print("Get accounts")

accounts = get_accounts(authorized_access_token)

print("accounts: ", accounts)

print("Get payees")

payees = get_payees(authorized_access_token)

print("payees: ", payees)

print("Bill pay")

bill_pay_response = bill_pay(authorized_access_token)

print("bill_pay_response: ", bill_pay_response)

print("Done")

