import sys

sys.path.append("../")

from api_client import bill_pay, get_accounts, get_payees
from behave import given, then, when


@given("I have a valid API access token with ReadAccountsDetail API consent.")
def step_impl(context):
    assert context.authorized_access_token is not None


@when("I call the accounts API endpoint.")
def step_impl(context):
    context.accounts_response = get_accounts(context.authorized_access_token)


@then("I receive a list of all accounts included in my Bank of APIs test data.")
def step_impl(context):
    assert context.accounts_response is not None


@given("I have a valid API access token with BillPayService API consent.")
def step_impl(context):
    assert context.authorized_access_token is not None


@when("I call the payees API endpoint providing a valid account ID.")
def step_impl(context):
    context.payees_response = get_payees(context.authorized_access_token)


@then("I receive a list of all payees associated with that account ID.")
def step_impl(context):
    assert context.payees_response is not None


@when(
    "I call the bill-pay-request API endpoint providing a valid payeeId ID and amount."
)
def step_impl(context):
    context.bill_pay_response = bill_pay(context.authorized_access_token)


@then("I receive a response containing a valid request referenceNumber.")
def step_impl(context):
    assert context.bill_pay_response is not None
    assert context.bill_pay_response["referenceNumber"] is not None
