Feature: BillPay

  As a member of the NatWest AI research team.
  I want to perform an exercise measuring developer productivity with and without AI-assisted code suggestions developing a client for the NatWest Bank of APIs Bill Pay service.
  So that I can compare the results and provide quantitive data.

  Scenario: List accounts.
    Given I have a valid API access token with ReadAccountsDetail API consent.
    When I call the accounts API endpoint.
    Then I receive a list of all accounts included in my Bank of APIs test data.

  Scenario: List payees.
    Given I have a valid API access token with BillPayService API consent.
    When I call the payees API endpoint providing a valid account ID.
    Then I receive a list of all payees associated with that account ID.

  Scenario: Pay Bill.
    Given I have a valid API access token with BillPayService API consent.
    When I call the bill-pay-request API endpoint providing a valid payeeId ID and amount.
    Then I receive a response containing a valid request referenceNumber.
