import sys

sys.path.append("../")

from api_client import (
    authorize_consent,
    exchange_authorization_code_for_access_token,
    get_access_token,
    grant_consent,
)


def before_feature(context, feature):
    context.access_token = get_access_token()
    context.consent_id = grant_consent(context.access_token)
    context.authorization_code = authorize_consent(context.consent_id)
    context.authorized_access_token = exchange_authorization_code_for_access_token(
        context.authorization_code
    )
