import os
import urllib.parse

import requests
from dotenv import load_dotenv

load_dotenv()

CLIENT_ID = os.getenv("CLIENT_ID")
CLIENT_SECRET = os.getenv("CLIENT_SECRET")
REDIRECT_URL = os.getenv("REDIRECT_URL")
PSU_USERNAME = os.getenv("PSU_USERNAME")
TRANSACTIONS_ACCOUNT_ID = os.getenv("TRANSACTIONS_ACCOUNT_ID")
PAYEE_ID = os.getenv("PAYEE_ID")


def get_access_token() -> str:
    data = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "client_credentials",
        "scope": "accounts",
    }
    try:
        r = requests.post(
            "https://ob.sandbox.natwest.com/token",
            data=data,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(
            f"Access token creation failed. Reponse from the server was: {r.json()}"
        )
    return r.json()["access_token"]


def grant_consent(access_token) -> str:
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {access_token}",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
        "Accept": "*/*",
    }
    payload = {
        "Data": {
            "Permissions": [
                "ReadAccountsDetail",
                "BillPayService",
            ]
        },
        "Risk": {},
    }

    try:
        r = requests.post(
            "https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/account-access-consents",
            json=payload,
            headers=headers,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(
            f"Grant consent failed. Reponse from the server was: {r.json()}"
        )
    return r.json()["Data"]["ConsentId"]


def authorize_consent(consent_id):
    headers = {
        "Content-Type": "application/json",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    }

    client_id = CLIENT_ID
    encoded_redirect_url = urllib.parse.quote_plus(REDIRECT_URL)
    consent_id = consent_id
    psu_username = PSU_USERNAME

    url = f"https://api.sandbox.natwest.com/authorize?client_id={client_id}&response_type=code id_token&scope=openid accounts&redirect_uri={encoded_redirect_url}&state=ABC&request={consent_id}&authorization_mode=AUTO_POSTMAN&authorization_username={psu_username}"

    r = requests.get(url, headers=headers)

    return r.json()["redirectUri"].split("#")[1].split("&")[0].replace("code=", "")


def exchange_authorization_code_for_access_token(authorization_code):
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    }

    data = {
        "grant_type": "authorization_code",
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "redirect_uri": REDIRECT_URL,
        "code": authorization_code,
    }

    try:
        r = requests.post(
            "https://ob.sandbox.natwest.com/token",
            data=data,
            headers=headers,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(
            f"Exchange authorization code for access token failed. Reponse from the server was: {r.json()}"
        )
    return r.json()["access_token"]


def get_accounts(authorized_access_token):
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {authorized_access_token}",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
        "Accept": "*/*",
    }

    try:
        r = requests.get(
            "https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts",
            headers=headers,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(f"Get accounts failed. Reponse from the server was: {r.json()}")
    return r.json()


def get_payees(authorized_access_token):
    headers = {
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "Authorization": f"Bearer {authorized_access_token}",
    }

    try:
        r = requests.get(
            f"https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts/{TRANSACTIONS_ACCOUNT_ID}/payees",
            headers=headers,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(f"Get payees failed. Reponse from the server was: {r.json()}")
    return r.json()


def bill_pay(authorized_access_token):
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {authorized_access_token}",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    }

    payload = {
        "payeeId": PAYEE_ID,
        "amount": "17.93",
    }

    try:
        r = requests.post(
            f"https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts/{TRANSACTIONS_ACCOUNT_ID}/bill-pay-request",
            json=payload,
            headers=headers,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(f"Bill pay failed. Reponse from the server was: {r.json()}")
    return r.json()
